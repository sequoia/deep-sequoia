+++
title = "example"
Categories = ["Development","GoLang"]
Tags = ["Development","golang"]
Description = ""
menu = "main"
date = "2019-05-17T10:37:17+02:00"

+++

# Example of the 6 different annotation formats provided

See [process page](../process) for formats description.

## Format Sequoia-S
See file: [`frwiki_50.1000_00754.surf.conll`](/example/frwiki_50.1000_00754.surf.conll):
![surf.conll](../example/frwiki_50.1000_00754.surf.conll.svg)

## Format Sequoia-SD
See file: [`frwiki_50.1000_00754.deep_and_surf.conll`](/example/frwiki_50.1000_00754.deep_and_surf.conll):
![deep_and_surf.conll](../example/frwiki_50.1000_00754.deep_and_surf.conll.svg)

## Format Sequoia-SPF
See file: [`frwiki_50.1000_00754.surf.parseme.frsemcor`](/example/frwiki_50.1000_00754.surf.parseme.frsemcor):
![surf.parseme.frsemcor](../example/frwiki_50.1000_00754.surf.parseme.frsemcor.svg)

## Format Sequoia-SDPF
See file: [`frwiki_50.1000_00754.deep_and_surf.parseme.frsemcor`](/example/frwiki_50.1000_00754.deep_and_surf.parseme.frsemcor)
![deep_and_surf.parseme.frsemcor](../example/frwiki_50.1000_00754.deep_and_surf.parseme.frsemcor.svg)

## Format Sequoia_UD-S
See file: [`frwiki_50.1000_00754-ud.conllu`](/example/frwiki_50.1000_00754-ud.conllu):
![ud.conllu](../example/frwiki_50.1000_00754-ud.conllu.svg)

## Format Sequoia_UD-SPF
See file: [`frwiki_50.1000_00754-ud.parseme.frsemcor`](/example/frwiki_50.1000_00754-ud.parseme.frsemcor):
![ud.parseme.frsemcor](../example/frwiki_50.1000_00754-ud.parseme.frsemcor.svg)


