+++
Tags = ["Development","golang"]
Description = ""
date = "2017-02-27T17:44:22+01:00"
title = "index"
menu = "main"
Categories = ["Development","GoLang"]

+++

# Deep-sequoia: A multilayer French corpus

**Deep-sequoia** is a corpus of French sentences annotated in several projects with a set of annotation layers.
It is freely available with the [LGPL-LR](../licence) License.
The latest version is the **9.2** released in October 2020 (see the [README file](https://gitlab.inria.fr/sequoia/deep-sequoia/-/blob/master/tags/sequoia-9.2/README-distrib.md))



### Annotation layers:

| Layer | Code | Project | Version | Guidelines |
|-------|:----:|-------|------------|------------|
| Surface syntax | **S** | [Sequoia](https://www.rocq.inria.fr/alpage-wiki/tiki-index.php?page=CorpusSequoia) (2012) | See [Sequoia](https://www.rocq.inria.fr/alpage-wiki/tiki-index.php?page=CorpusSequoia) | included in [deep syntax annotation guidelines](deep-sequoia-guide.pdf) (in French) |
| Deep syntax | **D** | [Deep-sequoia](https://deep-sequoia.inria.fr/) (2014) | Since 1.0 | [deep syntax annotation guidelines](deep-sequoia-guide.pdf) (in French) |
| MWEs and Named Entities | **P** | [PARSEME-FR](http://parsemefr.lis-lab.fr) (2019) | Since 9.0 | [PARSEME-FR guide](https://gitlab.lis-lab.fr/PARSEME-FR/PARSEME-FR-public/wikis/Guide-annotation-PARSEME_FR-chapeau) |
| Supersense annotation | **F** | [FRSEMCOR](https://frsemcor.github.io/FrSemCor) (2020) | Since 9.1 | [FRSEMCOR guide](https://github.com/FrSemCor/FrSemCor/blob/master/guideAnno-FR-SemCor.pdf) (in French) |

## References

### Initial version (constituency trees + surface dependencies)
 * **Marie Candito** and **Djamé Seddah**. (2012) [*Le corpus Sequoia : annotation syntaxique et exploitation pour l’adaptation d’analyseur par pont lexical*](https://hal.inria.fr/hal-00698938/document), Proceedings of TALN'2012, Grenoble, France.

 * **Marie Candito** and **Djamé Seddah**. (2012) [*Effectively long-distance dependencies in French: annotation and parsing evaluation*](https://hal.inria.fr/hal-00769625/document), Proceedings of TLT'11, 2012, Lisbon, Portugal.

### Deep-sequoia: deep syntactic annotations
 * **Marie Candito**, **Guy Perrier**, **Bruno Guillaume**, **Corentin Ribeyre**, **Karën Fort**, **Djamé Seddah** and **Éric de la Clergerie**. (2014) [*Deep Syntax Annotation of the Sequoia French Treebank.*](https://hal.inria.fr/hal-00969191v2/document) Proc. of LREC 2014, Reykjavic, Iceland.

 * **Guy Perrier**, **Marie Candito**, **Bruno Guillaume**, **Corentin Ribeyre**, **Karën Fort** and **Djamé Seddah**. (2014) [*Un schéma d’annotation en dépendances syntaxiques profondes pour le français.*](https://hal.inria.fr/hal-01054407/document) Proc. of TALN 2014, Marseille, France.

### PARSEME-FR: MWE and named entities annotation

 * **Marie Candito**, **Mathieu Constant**, **Carlos Ramisch**, **Agata Savary**, **Yannick Parmentier**, **Caroline Pasquer** and **Jean-Yves Antoine**. (2017) [*Annotation d'expressions polylexicales verbales en français*](https://hal.archives-ouvertes.fr/hal-01537880/document), Proc. of TALN 2017 - short papers, Orléans

 * **In preparation**: A French corpus annotated for multi-word expressions and named entities.

### FRSEMCOR: Supersens annotation

 * **Lucie Barque**, **Pauline Haas**, **Richard Huyghe**, **Delphine Tribout**, **Marie Candito**, **Benoît Crabbé** and **Vincent Segonne**. (2020) [*Annotating a French Corpus with Supersenses*](https://www.aclweb.org/anthology/2020.lrec-1.724/), Proc. of LREC 2020, Marseille, France.



