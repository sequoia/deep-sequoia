+++
Tags = ["Development","golang"]
Description = ""
date = "2019-03-19T15:36:55+01:00"
title = "process"
menu = "main"
Categories = ["Development","GoLang"]

+++

# Update process of the Sequoia resources

Several formats are available they are all built from a the single resource which contains all layers `sequoia.deep_and_surf.parseme.frsemcor`.
The figure below describes the different transformations that are applied to produce other formats.

![process](../doc/process.svg)

All files are available from the [Gitlab project](https://gitlab.inria.fr/sequoia/deep-sequoia).

Formats depend on:

 * presence of [PARSEME-FR](http://parsemefr.lif.univ-mrs.fr/doku.php) and [FRSEMCOR](https://frsemcor.github.io/FrSemCor) annotations:
  * with such annotations: suffix `parseme.frsemcor` and orange background
  * without: suffix `conll` (or `conllu`) and green background
 * Deep syntactic annotation:
  * with both surface and deep dependencies: basename is `sequoia.deep_and_surf`
  * with only surface dependencies: basename is `sequoia.surf`
 * Conversion to [Universal dependencies](http://universaldependencies.org/)
  * Native dependencies: basename is `sequoia`
  * Universal dependencies conversion: basename is `sequoia-ud`

The Graph Rewriting Systems (usable with [Grew](http://grew.fr)) are available:

 * `sequoia_proj.grs`: in the [Gitlab project](https://gitlab.inria.fr/sequoia/deep-sequoia) (folder `tools`)
 * `ssq_to_ud/main.grs`: in the [Gitlab project](https://gitlab.inria.fr/grew/SSQ_UD) (folder `grs`)

## Note about universal dependencies

In order to produce the final files for the UD project, two more steps are needed (not detailed here)

 * Adding the misc feature `SpaceAfter=No` when needed (using [udapi tool](https://udapi.github.io))
 * Splitting into three subfiles `dev`, `test` and `train`