+++
date = "2018-03-26T13:40:35+02:00"
title = "versions"
menu = "main"
Categories = ["Development","GoLang"]
Tags = ["Development","golang"]
Description = ""

+++

# Versions

## Releases
| Version |   Date     | UD-version | Description  |
|:-------:|:----------:|:----------:|-----------|
| [9.2](http://deep-sequoia.inria.fr/download/sequoia-9.2.tgz) | 2020.10.05 | [2.7] | Few corrections of lemmas and FRSEMCOR annotations |
| [9.1](http://deep-sequoia.inria.fr/download/sequoia-9.1.tgz) | 2020.06.10 | [2.6](http://hdl.handle.net/11234/1-3226) | FRSEMCOR annotation |
| [9.0](http://deep-sequoia.inria.fr/download/sequoia-9.0.tgz) | 2019.05.17 | [2.4](http://hdl.handle.net/11234/1-2988) | MWE & NE anntations |
| [8.3](http://deep-sequoia.inria.fr/download/sequoia-8.3.tgz) | 2018.11.19 | [2.3](http://hdl.handle.net/11234/1-2895) | Fix some tokenisations |
| [8.2](http://deep-sequoia.inria.fr/download/sequoia-8.2.tgz) | 2018.03.16 | [2.2](http://hdl.handle.net/11234/1-2837) | Fix errors and improve annotation consistency |
| [8.1](http://deep-sequoia.inria.fr/download/sequoia-8.1.tgz) | 2017.10.21 | [2.1](http://hdl.handle.net/11234/1-2515) | Fix errors and improve annotation consistency |
| [8.0](http://deep-sequoia.inria.fr/download/sequoia-8.0.tgz) | 2017.03.13 | [2.0](http://hdl.handle.net/11234/1-1983) | Fix errors, change encoding of fixed expressions (see below) |
| [7.0](http://deep-sequoia.inria.fr/download/sequoia-7.0.tgz) | 2015.11.13 | | Fix errors by systematic search of inconsistencies in annotation.|
| [1.1](http://deep-sequoia.inria.fr/download/deep-sequoia-1.1.conll) | 2014.06.05 | | Fix some lemmas, fix 3 sentences with multiple surface roots |
| [1.0](http://deep-sequoia.inria.fr/download/deep-sequoia-1.0.conll) | 2014.05.29 | | First release with deep relations (aligned with [Sequioa 6.0](https://gforge.inria.fr/frs/download.php/file/34495/sequoia-corpus-v6.0.tgz)) |

## Development version
The current version is available as the `master` branch on the [Gitlab project](https://gitlab.inria.fr/sequoia/deep-sequoia) and the 6 versions of the corpus described in [process page](../process) are available on **Grew_match**:

 * [Sequoia_S](http://match.grew.fr?corpus=Sequoia_S@current): Current version of Native Sequoia with only Surface layer
 * [Sequoia_SD](http://match.grew.fr?corpus=Sequoia_SD@current): Current version of Native Sequoia with Surface and Deep layers
 * [Sequoia_SPF](http://match.grew.fr?corpus=Sequoia_SPF@current): Current version of Native Sequoia with Surface, Parseme and FrSemCor layers
 * [Sequoia_SDPF](http://match.grew.fr?corpus=Sequoia_SDPF@current): Current version of Native Sequoia with all layers
 * [Sequoia_UD-S](http://match.grew.fr?corpus=Sequoia_UD-S@current): Conversion to UD of Sequoia with only Surface layer
 * [Sequoia_UD-SPF](http://match.grew.fr?corpus=Sequoia_UD-SPF@current): Conversion to UD of Sequoia with Surface, Parseme and FrSemCor layers




# Notes

## Earlier versions
Earlier version of the Sequoia corpus (before deep syntax annotation) are described [here](https://www.rocq.inria.fr/alpage-wiki/tiki-index.php?page=CorpusSequoia) and are available for download [here](https://gforge.inria.fr/frs/?group_id=3597).

## Version numbers
In the 2015 release, the version number was set to **7.0** to be aligned with previous releases of the **Sequoia** corpus (before introduction of deep-dependencies).


## UD Versions
Since 2017, the Sequoia corpus (surface only) is also available in Universal Dependency format and is released as one of the UD corpora named
[UD_French-Sequoia](https://github.com/UniversalDependencies/UD_French-Sequoia/blob/dev/README.md).

## Encoding of fixed expressions
In version **7.0** and previous, fixed expressions are encoded in a single token with `_` symbol as a word separator.
Since version **8.0** these expressions are represented by several tokens linked with `dep_cpd` relations.
For instance, in the two figures below, the sentence `annodis.er_00106` is given with its annotation in Sequoia **7.0** and **8.0**.

![example 7.0](../S7.svg)

![example 8.0](../S8.svg)
