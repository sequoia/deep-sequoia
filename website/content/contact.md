+++
Description = ""
menu = "main"
Categories = ["Development","GoLang"]
Tags = ["Development","golang"]
date = "2018-03-26T13:34:37+02:00"
title = "contact"

+++

# Contact

For any remark, suggestion or correction to the corpus:

 * You can send a mail at [deep-sequoia@inria.fr](mailto:deep-sequoia@inria.fr).
 * You can fill a new issue on [Gitlab](https://gitlab.inria.fr/sequoia/deep-sequoia/issues) (you have to [Register](https://gitlab-account.inria.fr/) before)

If you refer to a precise sentence of the corpus, please try to give the identifier of the sentence, i.e. something like:

  * `annodis.er_00xxx`
  * `emea-fr-dev_00xxx`
  * `emea-fr-test_00xxx`
  * `Europar.550_00xxx`
  * `frwiki_50.1000_00xxx`
