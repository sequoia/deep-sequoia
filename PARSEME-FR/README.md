# Sequoia with PARSEME-FR annotations
The two files below were produced by projection of the annotation PARSEME-FR done on UD_French-Sequoia.

 * `sequoia-ftb-8.3.deep_and_surf.cupt_parsemefr`
 * `sequoia-ftb-8.3.surf.cupt_parsemefr`