% include "sequoia.dom"


rule deep_subj {
  pattern { X -[S:suj:_]-> IL; e:X -[kind=deep,2=suj]-> Y; Y[!deep_subj] }
  commands { Y.deep_subj = y; }
}

% ====================================================================================================
% le package [rm_deep] efface les relations D:... %
package rm_deep {
  rule r { pattern { e: X -[re"D:.*"]-> Y} commands { del_edge e } }
}

% ====================================================================================================
% le package [rm_surf] efface les relations S:... %
package rm_surf {
  rule r { pattern { e: X -[re"S:.*"]-> Y} commands { del_edge e } }
}

% ====================================================================================================
% le package [rm_meddle] efface les relations I:... %
package rm_meddle {
  rule r { pattern { e: X -[re"I:.*"]-> Y} commands { del_edge e } }
}

% ====================================================================================================
% le package [surf_final] remplace efface les relations S:x:y par x %
package surf_final {
  rule p_obj_o       { pattern { e:X -[S:p_obj.o]-> Y }       commands { del_edge e; add_edge X-[p_obj.o]-> Y } }
  rule mod           { pattern { e:X -[S:mod]-> Y }           commands { del_edge e; add_edge X-[mod]-> Y } }
  rule aff           { pattern { e:X -[S:aff]-> Y }           commands { del_edge e; add_edge X-[aff]-> Y } }
  rule ats_ats       { pattern { e:X -[S:ats:ats]-> Y }       commands { del_edge e; add_edge X-[ats]-> Y } }
  rule arg           { pattern { e:X -[S:arg]-> Y }           commands { del_edge e; add_edge X-[arg]-> Y } }
  rule mod_app       { pattern { e:X -[S:mod.app]-> Y }       commands { del_edge e; add_edge X-[mod.app]-> Y } }
  rule coord         { pattern { e:X -[S:coord]-> Y }         commands { del_edge e; add_edge X-[coord]-> Y } }
  rule dep_coord     { pattern { e:X -[S:dep.coord]-> Y }     commands { del_edge e; add_edge X-[dep.coord]-> Y } }
  rule ponct         { pattern { e:X -[S:ponct]-> Y }         commands { del_edge e; add_edge X-[ponct]-> Y } }
  rule aux_tps       { pattern { e:X -[S:aux.tps]-> Y }       commands { del_edge e; add_edge X-[aux.tps]-> Y } }
  rule aux_pass      { pattern { e:X -[S:aux.pass]-> Y }      commands { del_edge e; add_edge X-[aux.pass]-> Y } }
  rule aux_caus      { pattern { e:X -[S:aux.caus]-> Y }      commands { del_edge e; add_edge X-[aux.caus]-> Y } }
  rule obj_p         { pattern { e:X -[S:obj.p]-> Y }         commands { del_edge e; add_edge X-[obj.p]-> Y } }
  rule obj_cpl       { pattern { e:X -[S:obj.cpl]-> Y }       commands { del_edge e; add_edge X-[obj.cpl]-> Y } }
  rule suj_suj       { pattern { e:X -[S:suj:suj]-> Y }       commands { del_edge e; add_edge X-[suj]-> Y } }
  rule suj_obj       { pattern { e:X -[S:suj:obj]-> Y }       commands { del_edge e; add_edge X-[suj]-> Y } }
  rule obj_obj       { pattern { e:X -[S:obj:obj]-> Y }       commands { del_edge e; add_edge X-[obj]-> Y } }
  rule a_obj_a_obj   { pattern { e:X -[S:a_obj:a_obj]-> Y }   commands { del_edge e; add_edge X-[a_obj]-> Y } }
  rule a_obj_suj     { pattern { e:X -[S:a_obj:suj]-> Y }     commands { del_edge e; add_edge X-[a_obj]-> Y } }
  rule de_obj_de_obj { pattern { e:X -[S:de_obj:de_obj]-> Y } commands { del_edge e; add_edge X-[de_obj]-> Y } }
  rule pobj_agt_suj  { pattern { e:X -[S:p_obj.agt:suj]-> Y } commands { del_edge e; add_edge X-[p_obj.agt]-> Y } }
  rule suj_void      { pattern { e:X -[S:suj:_]-> Y }         commands { del_edge e; add_edge X-[suj]-> Y } }

  rule aff_demsuj    { pattern { e:X -[S:aff.demsuj]-> Y }    commands { del_edge e; add_edge X-[aff.demsuj]-> Y } }
  rule dep           { pattern { e:X -[S:dep]-> Y }           commands { del_edge e;  add_edge X-[dep]-> Y } }
  rule det           { pattern { e:X -[S:det]-> Y }           commands { del_edge e; add_edge X-[det]-> Y } }
  rule obj_suj       { pattern { e:X -[S:obj:suj]-> Y }       commands { del_edge e; add_edge X-[obj]-> Y } }
  rule mod_rel       { pattern { e:X -[S:mod.rel]-> Y }       commands { del_edge e; add_edge X-[mod.rel]-> Y } }
  rule mod_inc       { pattern { e:X -[S:mod.inc]-> Y }       commands { del_edge e; add_edge X-[mod.inc]-> Y } }
  rule dis_suj       { pattern { e:X -[S:dis:suj]-> Y }       commands { del_edge e; add_edge X-[dis]-> Y } }
  rule dis_obj       { pattern { e:X -[S:dis:obj]-> Y }       commands { del_edge e; add_edge X-[dis]-> Y } }
  rule root          { pattern { e:X -[S:root]-> Y }          commands { del_edge e; add_edge X-[root]-> Y } }
} % end package surf_final

% ====================================================================================================
% le package [final] remplace efface les relations x:y par x %
package final {
  rule suj_suj { pattern { e:X -[suj:suj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule ats_ats { pattern { e:X -[ats:ats]-> Y } commands { del_edge e; add_edge X-[ats]-> Y } }
  rule ats_ato { pattern { e:X -[ats:ato]-> Y } commands { del_edge e; add_edge X-[ats]-> Y } }
  rule ato_ato { pattern { e:X -[ato:ato]-> Y } commands { del_edge e; add_edge X-[ato]-> Y } }
  rule suj_obj { pattern { e:X -[suj:obj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule obj_obj { pattern { e:X -[obj:obj]-> Y } commands { del_edge e; add_edge X-[obj]-> Y } }
  rule a_obj_a_obj { pattern { e:X -[a_obj:a_obj]-> Y } commands { del_edge e; add_edge X-[a_obj]-> Y } }
  rule de_obj_de_obj { pattern { e:X -[de_obj:de_obj]-> Y } commands { del_edge e; add_edge X-[de_obj]-> Y } }
  rule a_obj_suj { pattern { e:X -[a_obj:suj]-> Y } commands { del_edge e; add_edge X-[a_obj]-> Y } }
  rule pobj_agt_suj { pattern { e:X -[p_obj.agt:suj]-> Y } commands { del_edge e; add_edge X-[p_obj.agt]-> Y } }
  rule obj_suj { pattern { e:X -[obj:suj]-> Y } commands { del_edge e; add_edge X-[obj]-> Y } }
  rule suj_void { pattern { e:X -[suj:_]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule suj_argc { pattern { e:X -[suj:argc]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule dis_dis { pattern { e:X -[dis:dis]-> Y } commands { del_edge e; add_edge X-[dis]-> Y } }
  rule dis_suj { pattern { e:X -[dis:suj]-> Y } commands { del_edge e; add_edge X-[dis]-> Y } }
  rule dis_obj { pattern { e:X -[dis:obj]-> Y } commands { del_edge e; add_edge X-[dis]-> Y } }

} % end package final

% ====================================================================================================
% le package [deep_canonical] remplace efface les relations D:x:y par y %
package deep_canonical {
  rule p_obj_o { pattern { e:X -[D:p_obj.o]-> Y } commands { del_edge e; add_edge X-[p_obj.o]-> Y } }
  rule mod { pattern { e:X -[D:mod]-> Y } commands { del_edge e; add_edge X-[mod]-> Y } }
  rule ats_ats { pattern { e:X -[D:ats:ats]-> Y } commands { del_edge e; add_edge X-[ats]-> Y } }
  rule arg { pattern { e:X -[D:arg]-> Y } commands { del_edge e; add_edge X-[arg]-> Y } }
  rule arg_comp { pattern { e:X -[D:arg.comp]-> Y } commands { del_edge e; add_edge X-[arg.comp]-> Y } }
  rule arg_cons { pattern { e:X -[D:arg.cons]-> Y } commands { del_edge e; add_edge X-[arg.cons]-> Y } }
  rule mod_comp { pattern { e:X -[D:mod.comp]-> Y } commands { del_edge e; add_edge X-[mod.comp]-> Y } }
  rule mod_super { pattern { e:X -[D:mod.super]-> Y } commands { del_edge e; add_edge X-[mod.super]-> Y } }
  rule mod_app { pattern { e:X -[D:mod.app]-> Y } commands { del_edge e; add_edge X-[mod.app]-> Y } }
  rule mod_rel { pattern { e:X -[D:mod.rel]-> Y } commands { del_edge e; add_edge X-[mod.rel]-> Y } }
  rule mod_inc { pattern { e:X -[D:mod.inc]-> Y } commands { del_edge e; add_edge X-[mod.inc]-> Y } }
  rule mod_rel_part { pattern { e:X -[D:mod.rel.part]-> Y } commands { del_edge e; add_edge X-[mod.rel.part]-> Y } }
  rule coord { pattern { e:X -[D:coord]-> Y } commands { del_edge e; add_edge X-[coord]-> Y } }
  rule dep_coord { pattern { e:X -[D:dep.coord]-> Y } commands { del_edge e; add_edge X-[dep.coord]-> Y } }
  rule dep_de { pattern { e:X -[D:dep.de]-> Y } commands { del_edge e; add_edge X-[dep.de]-> Y } }
  rule obj_p { pattern { e:X -[D:obj.p]-> Y } commands { del_edge e; add_edge X-[obj.p]-> Y } }
  rule suj_suj { pattern { e:X -[D:suj:suj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule suj_obj { pattern { e:X -[D:suj:obj]-> Y } commands { del_edge e; add_edge X-[obj]-> Y } }
  rule obj_obj { pattern { e:X -[D:obj:obj]-> Y } commands { del_edge e; add_edge X-[obj]-> Y } }
  rule a_obj_a_obj { pattern { e:X -[D:a_obj:a_obj]-> Y } commands { del_edge e; add_edge X-[a_obj]-> Y } }
  rule de_obj_de_obj { pattern { e:X -[D:de_obj:de_obj]-> Y } commands { del_edge e; add_edge X-[de_obj]-> Y } }
  rule a_obj_suj { pattern { e:X -[D:a_obj:suj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule pobj_agt_suj { pattern { e:X -[D:p_obj.agt:suj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule obj_suj { pattern { e:X -[D:obj:suj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule suj_void { pattern { e:X -[D:suj:_]-> Y } commands { del_edge e} }
  rule suj_argc { pattern { e:X -[D:suj:argc]-> Y } commands { del_edge e; add_edge X-[argc]-> Y } }

  rule ponct { pattern { e:X -[D:ponct]-> Y } commands { del_edge e; add_edge X-[ponct]-> Y } }
  rule dep { pattern { e:X -[D:dep]-> Y } commands { del_edge e; add_edge X-[dep]-> Y } }
  rule det { pattern { e:X -[D:det]-> Y } commands { del_edge e; add_edge X-[det]-> Y } }
  rule obj_cpl { pattern { e:X -[D:obj.cpl]-> Y } commands { del_edge e; add_edge X-[obj.cpl]-> Y } }
  rule dis_suj { pattern { e:X -[D:dis:suj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule dis_obj { pattern { e:X -[D:dis:obj]-> Y } commands { del_edge e; add_edge X-[obj]-> Y } }
  rule root { pattern { e:X -[D:root]-> Y } commands { del_edge e; add_edge X-[root]-> Y } }

} % end package deep_canonical

% ====================================================================================================
% le package [canonical] remplace les relations x:y par y  %
package canonical {
  rule suj_suj       { pattern { e:X -[suj:suj]-> Y }       commands { del_edge e; add_edge X-[suj]-> Y } }
  rule ats_ats       { pattern { e:X -[ats:ats]-> Y }       commands { del_edge e; add_edge X-[ats]-> Y } }
  rule ats_ato       { pattern { e:X -[ats:ato]-> Y }       commands { del_edge e; add_edge X-[ato]-> Y } }
  rule ato_ato       { pattern { e:X -[ato:ato]-> Y }       commands { del_edge e; add_edge X-[ato]-> Y } }
  rule suj_obj       { pattern { e:X -[suj:obj]-> Y }       commands { del_edge e; add_edge X-[obj]-> Y } }
  rule obj_obj       { pattern { e:X -[obj:obj]-> Y }       commands { del_edge e; add_edge X-[obj]-> Y } }
  rule a_obj_a_obj   { pattern { e:X -[a_obj:a_obj]-> Y }   commands { del_edge e; add_edge X-[a_obj]-> Y } }
  rule de_obj_de_obj { pattern { e:X -[de_obj:de_obj]-> Y } commands { del_edge e; add_edge X-[de_obj]-> Y } }
  rule a_obj_suj     { pattern { e:X -[a_obj:suj]-> Y }     commands { del_edge e; add_edge X-[suj]-> Y } }
  rule pobj_agt_suj  { pattern { e:X -[p_obj.agt:suj]-> Y } commands { del_edge e; add_edge X-[suj]-> Y } }
  rule obj_suj       { pattern { e:X -[obj:suj]-> Y }       commands { del_edge e; add_edge X-[suj]-> Y } }
  rule suj_void      { pattern { e:X -[suj:_]-> Y }         commands { del_edge e } }
  rule suj_argc      { pattern { e:X -[suj:argc]-> Y }      commands { del_edge e; add_edge X-[argc]-> Y } }
  rule dis_dis       { pattern { e:X -[dis:dis]-> Y }       commands { del_edge e; add_edge X-[dis]-> Y } }
  rule dis_suj       { pattern { e:X -[dis:suj]-> Y }       commands { del_edge e; add_edge X-[suj]-> Y } }
  rule dis_obj       { pattern { e:X -[dis:obj]-> Y }       commands { del_edge e; add_edge X-[obj]-> Y } }
} % end package canonical

% ====================================================================================================
% le package [remove_deep_features] supprime les traits deep-only void=y %
package remove_deep_features {
  rule void_y_cl      { pattern { X[void=y, upos=CL] }  commands { del_feat X.void; X.impers=y} }
  rule void_y         { pattern { X[void=y, upos<>CL] } commands { del_feat X.void} }
  rule dl             { pattern { X[dl=*] }             commands { del_feat X.dl } }
  rule dm             { pattern { X[dm=*] }             commands { del_feat X.dm } }
  rule diat           { pattern { X[diat=*] }           commands { del_feat X.diat } }
  rule def            { pattern { X[def=*] }            commands { del_feat X.def } }
  rule intrinsimp     { pattern { X[intrinsimp=*] }     commands { del_feat X.intrinsimp } }
  rule se             { pattern { X[se=*] }             commands { del_feat X.se } }
  rule cltype         { pattern { X[cltype=*] }         commands { del_feat X.cltype } }
  rule lexicalisation { pattern { X[lexicalisation=*] } commands { del_feat X.lexicalisation } }
} % end package remove_deep_features

% ====================================================================================================
% le package [sentid] reporte le trait sentid sur le premier mot non vide pour éviter de perdre l'identifiant %
package sentid {
  rule report {
    pattern { X[sentid=*, void=y]; Y[!void]; X.position < Y.position}
    without { Z [!void]; X.position < Z.position; Z.position < Y.position }
    commands { Y.sentid = X.sentid; del_feat X.sentid }
  }
} % end package sentid

% ====================================================================================================
% le package [clean] supprime les nœuds avec void=y %
package clean {
  rule clean { pattern { X[void=y] } commands { del_node X } }
} % end package clean

% ====================================================================================================
% le package [clean] rattache les nœuds avec void=y au pseudo-noeud 0 %
package void {
  rule clean {
    pattern { X[void=y]; ZERO[!phon]}
    without { ZERO -[void]-> X }
    commands { add_edge ZERO -[void]-> X }
  }
} % end package clean

% ====================================================================================================
strat surf {
  Seq (
    Onf (deep_subj),
    Onf (rm_deep),
    Onf (rm_meddle),
    Onf (surf_final),
    Onf (final),
    Onf (remove_deep_features),
  )
}

strat deep {
  Seq (
    Onf(rm_surf),
    Onf(rm_meddle),
    Onf(deep_canonical),
    Onf(canonical),
    Onf(void),
  )
}

strat deep_clean {
  Seq (
    Onf(rm_surf),
    Onf(rm_meddle),
    Onf(deep_canonical),
    Onf(canonical),
    Onf(sentid),
    Onf(clean),
  )
}
